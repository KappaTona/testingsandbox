#include <gmock/gmock-actions.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include <memory>
#include <string>
#include "impl.h"

using ::testing::HasSubstr;

TEST(TestImpl, ImplAddFooLimited00eq0)
{
    int left = 0, right = 0, limit = 1, expected_result = 0;
    auto obj_instance = std::make_unique<Impl>(limit);

    int operation_result = obj_instance->add_foo_limited(left, right);

    EXPECT_EQ(operation_result, expected_result);
}

TEST(TestImpl, ImplSubFooLimited00eq0)
{
    int left = 0, right = 0, limit = 1, expected_result = 0;
    auto obj_instance = std::make_unique<Impl>(limit);

    int operation_result = obj_instance->sub_foo_limited(left, right);

    EXPECT_EQ(operation_result, expected_result);
}

TEST(TestImpl, ImplAddFooLimitedThrowsException)
{
    int limit = 0;
    auto obj_instance = std::make_unique<Impl>(limit);
    ASSERT_NE(obj_instance, nullptr);

    EXPECT_THROW({
    try {
        obj_instance->add_foo_limited(1, 0);
    } catch (const InterfaceException& e)
    {
        auto msg = e.what();
        EXPECT_THAT(msg, HasSubstr("add")) << "Exception should contain the operation name\n";
        EXPECT_THAT(msg, HasSubstr(std::to_string(limit)))
            << "Exception should contain the limit: " << limit << std::endl;

        // rethrow so we can verify Exception type as well
        throw;
    }
    }, InterfaceException);
}

TEST(TestImpl, ImplSubFooLimitedThrowsException)
{
    int limit = 3;
    auto obj_instance = std::make_unique<Impl>(limit);
    ASSERT_NE(obj_instance, nullptr);

    EXPECT_THROW({
    try {
        obj_instance->sub_foo_limited(3, 12);
    } catch (const InterfaceException& e)
    {
        auto msg = e.what();
        EXPECT_THAT(msg, HasSubstr("sub")) << "Exception should contain operation name\n";
        EXPECT_THAT(msg, HasSubstr(std::to_string(limit)))
            << "Exception should contain the limit: " << limit << std::endl;

        // rethrow so we can verify Exception type as well
        throw;
    }
    }, InterfaceException);
}

class Foo
{
public:
    virtual ~Foo()=default;
    virtual int addFoo(int a, int b) = 0;
};

using ::testing::Throw;
class MockImpl: public Foo
{
public:
    MOCK_METHOD(int, addFoo, (int, int), (override));
};

TEST(TestImpl, TrySideEffect)
{
    MockImpl mockF;
    const int limit = 9;
    EXPECT_THROW({
        try {
           EXPECT_CALL(mockF, addFoo(1,2)).WillOnce(
                Throw(InterfaceException{"add_foo_limited reached limit 9\n"}));
           mockF.addFoo(1,2);
        } catch (const InterfaceException& e)
        {
            auto msg = e.what();
            EXPECT_THAT(msg, HasSubstr("add")) << "Exception should contain operation name\n";
            EXPECT_THAT(msg, HasSubstr(std::to_string(limit)))
                << "Exception should contain the limit: " << limit << std::endl;

            // rethrow so we can verify Exception type as well
            throw;
        }
    }, InterfaceException);
}
