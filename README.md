#### Requiremnts Linux
- cmake, gtest, gcc, make, gcovr

#### CMake targets
- `test_one` for the tests
- `test_one_cov` for coverage

#### Build
- If code coverage needed
    - `cmake -S. -DENABLE_CODECOVERAGE -B_build`
    - `make -C _build` or `cmake --build _build`

#### Run
By default on `UNIX` if you isse the Build steps above and `${ENABLE_CODECOVERAGE}` is ON,
a generated HTML code coverage will be found in `${CMAKE_BINARY_DIR}/test_one_cov/index.html`

