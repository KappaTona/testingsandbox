#ifndef one_interface_h
#define one_interface_h
#include <memory>
#include <stdexcept>
#include <string>

class InterfaceException : public std::exception
{
    std::string how_;
public:
    const char* what() const noexcept { return how_.c_str(); }
    InterfaceException(std::string how): how_{how} {}
};


class Interface
{
public:
    virtual ~Interface() = default;

    virtual int add_foo_limited(int a, int b) = 0;
    virtual int sub_foo_limited(int a, int b) = 0;
};


#endif //one_interface_h
