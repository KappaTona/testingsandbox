#ifndef impl_h_
#define impl_h_
#include "one_interface.h"


class Impl: public Interface
{
    int result_limit_;
    void check_limit(int exceeded_limit, std::string msg);
public:
    Impl(int result_limit);

    // limit is upper positive limit
    int add_foo_limited(int a, int b) override;

    // limit is bottom negative limit as of `result_limit_ * -1`
    int sub_foo_limited(int a, int b) override;
};
    
#endif //impl_h_
