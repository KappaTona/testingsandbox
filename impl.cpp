#include "impl.h"
#include <string>

Impl::Impl(int limit): result_limit_{limit} {}

int Impl::add_foo_limited(int a, int b)
{
    int tmp_result = a + b;
    check_limit(tmp_result, "Impl::add_foo_limited exceeded given limit: "
        + std::to_string(result_limit_) + "\n");

    return tmp_result;
}

int Impl::sub_foo_limited(int a, int b)
{
    int tmp_result = a - b;
    check_limit(-tmp_result, "Impl::sub_foo_limited exceeded given negative limit: "
        + std::to_string(-result_limit_) + "\n");

    return tmp_result;
}

void Impl::check_limit(int exceeded_limit, std::string msg)
{
    if (exceeded_limit > result_limit_)
        throw InterfaceException{msg};
}
