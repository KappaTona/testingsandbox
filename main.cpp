#include "impl.h"
#include <iostream>
#include <memory>
#include <string>
class Impl;


int main()
{
    std::unique_ptr<Interface> obj_instance = std::make_unique<Impl>(5);

    try {
        int left = 0, right = 4;
        int result = obj_instance->add_foo_limited(0, 4);

        std::string msg = std::string{"Succesfully added: "}.append(std::to_string(left)
            ).append(" ").append(std::to_string(right)).append(" = "
            ).append(std::to_string(result));
        std::cout << msg << std::endl;

        result = obj_instance->add_foo_limited(0, 7);

    } catch (const InterfaceException& e)
    {
        std::cerr << e.what() << std::endl;
    }
}
